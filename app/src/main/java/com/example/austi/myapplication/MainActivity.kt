package com.example.austi.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Text
import java.io.Console
import java.security.Principal
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Attach items to compound spinner
        val spinner: Spinner = findViewById(R.id.compounds)
        // Create an ArrayAdapter using string array and a default spinner layout
        ArrayAdapter.createFromResource(
                this,
                R.array.compound_choices,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        // Set min/max on NumberPicker
        val picker: NumberPicker = findViewById(R.id.years)
        picker.minValue = 0
        picker.maxValue = 100

        val principal = findViewById<TextView>(R.id.principal)
        val rate = findViewById<TextView>(R.id.rate)
        val amount = findViewById<TextView>(R.id.amount)

        val btn_click = findViewById<Button>(R.id.calcButton)
        btn_click.setOnClickListener {
            val graph = findViewById<GraphView>(R.id.graph)
            graph.removeAllSeries()
            val compounds_str = spinner.selectedItem.toString()
            var n = 365

            if(compounds_str == "Annually") {
                n = 1
            }
            else if(compounds_str == "Quarterly") {
                n = 4
            }
            else if(compounds_str == "Monthly") {
                n = 12
            }

            val amt = calculateCompountInterest(
                    java.lang.Double.parseDouble(principal.text.toString()),
                    java.lang.Double.parseDouble(rate.text.toString()) / 100,
                    picker.value,
                    n
            )

            amount.text = amt.toString()
        }
    }


    fun calculateCompountInterest(principal: Double, rate: Double, time: Int, compounds: Int): Double {

        val graph = findViewById<GraphView>(R.id.graph)
        var series = LineGraphSeries<DataPoint>()
        graph.addSeries(series)

        // add graph data points
        for (i in 0..time + 1 ) {
            val amt = (principal * (1 + (rate/compounds)).pow(compounds * i))
            series.appendData(DataPoint(i.toDouble(), amt), false, 5000)
        }

        val amount = (principal * (1 + (rate/compounds)).pow(compounds * time))

        return amount
    }


}
